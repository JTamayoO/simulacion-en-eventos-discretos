interface Iterador<T>{
	public boolean hasNext();
	public T next();
}