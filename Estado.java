import java.util.ArrayList;

class Estado {
	private ArrayList<VariableEstado> variables_estado;
	public Estado(){
		variables_estado = new ArrayList<VariableEstado>();
	}
	public void setVariablesEstado(ArrayList<VariableEstado> variables_estado){
		this.variables_estado = variables_estado;
	}
	
	public ArrayList<VariableEstado> getVariables_estado() {
		return variables_estado;
	}
	
	public void agregarVariableEstado(VariableEstado variable){
		variables_estado.add(variable);
	}
	public String toString(){
		StringBuilder estado = new StringBuilder();
		for(VariableEstado variable : variables_estado){
			estado.append(variable.toString());
			estado.append("\r\n");
		}
		return estado.toString();
	}
}
