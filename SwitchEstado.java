class SwitchEstado implements ModificadorEstado{
	private VariableEstado variable_estado;
	private Double valor_on;

	public SwitchEstado(VariableEstado variable){
		variable_estado = variable;
		valor_on = 0.0;
	}

	public void modificar(){
		variable_estado.setValor( 1 - valor_on);
		valor_on = variable_estado.getValor();
	}

	public void setVariableEstado(VariableEstado variable){
		variable_estado = variable;
	}

	public void setValorOn(Double valor_switch){
		valor_on = valor_switch;
	}
}