class DistribucionUniforme implements Distribucion {
	private Double minimum;
	private Double maximum;

	public DistribucionUniforme(){
		minimum = 0.0;
		maximum = 1.0;
	}

	public Double generar(){
		return (Math.random() * (maximum - minimum)) + minimum;
	}
}