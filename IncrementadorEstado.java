class IncrementadorEstado implements ModificadorEstado{
	private VariableEstado variable_estado;
	private Double valor_incremento;

	public IncrementadorEstado(VariableEstado variable){
		variable_estado = variable;
		valor_incremento = 1.0;
	}

	public void modificar(){
		variable_estado.setValor(variable_estado.getValor() + valor_incremento);
	}

	public void setVariableEstado(VariableEstado variable){
		variable_estado = variable;
	}

	public void setValorIncremento(Double valor_incrementar){
		valor_incremento = valor_incrementar;
	}
}