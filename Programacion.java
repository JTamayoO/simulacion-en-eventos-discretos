import java.util.ArrayList;

class Programacion {
	private ArrayList<Condicion> condiciones;
	private Evento evento;
	private Distribucion generador;

	public Programacion(){
		condiciones = new ArrayList<Condicion>();
		evento = new Evento();
		generador = new DistribucionNill();
	}

	public boolean cumpleCondiciones(){
		boolean programable = true;
		for(Condicion condicion : condiciones){
			if(!condicion.evaluar()){
				programable = false; 
				break;
			}
		}
		return programable;
	}

	public void agregarCondicion(Condicion condicion){
		condiciones.add(condicion);
	}

	public void setEvento(Evento evento){
		this.evento = evento;
	}
	
	public void desprogramar(){
		this.evento.setTiempo(Double.POSITIVE_INFINITY);
	}

	public void programar(Double tiempo_actual_simulacion){
		evento.setTiempo(tiempo_actual_simulacion + generador.generar());
	}

	public void setDistribucion(Distribucion distribucion){
		this.generador = distribucion;
	}
	public void setCondiciones(ArrayList<Condicion> condiciones){
		this.condiciones = condiciones;
	}

}