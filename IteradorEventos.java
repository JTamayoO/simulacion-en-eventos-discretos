import java.util.ArrayList;

class IteradorEventos implements Iterador{
	private ArrayList<Evento> eventos;
	public IteradorEventos(ArrayList<Evento> eventos){
		this.eventos = eventos;
	}
	public boolean hasNext(){
		return false;
	}
	public Evento next(){
		double min_tiempo = Double.POSITIVE_INFINITY;
		Evento siguiente_evento = null;
		for(Evento evento : eventos){
			Double tiempo_evento = evento.getTiempo();
			if(tiempo_evento < min_tiempo){
				siguiente_evento = evento;
				min_tiempo = tiempo_evento;
			}
		}
		if(siguiente_evento==null){
			//Throw Exception
		}
		return siguiente_evento;
	}
}