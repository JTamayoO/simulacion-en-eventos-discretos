class DistribucionExponencial implements Distribucion {
	private Double lambda;

	public DistribucionExponencial(Double lambda){
		this.lambda = lambda;
	}

	public Double generar(){
		return -Math.log(1 - Math.random())/ lambda;
	}
}