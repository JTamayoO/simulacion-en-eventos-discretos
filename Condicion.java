class Condicion{

	public static int MAYOR = 1;
	public static int MENOR = -1;
	public static int IGUAL = 0;


	private VariableEstado variable_estado;
	private Double resultado;
	private int esperado;

	public Condicion(){
		variable_estado = new VariableEstado();
		resultado = 0.0;
		esperado = 0;
	}

	public boolean evaluar(){
		return (variable_estado.compareTo(resultado) == esperado);
	}

	public void setVariableEstado(VariableEstado var_estado){
		variable_estado = var_estado;
	}

	public void setResultado(Double resultado_a_comparar){
		resultado = resultado_a_comparar;
	}
	public void setEsperado(int valor_esperado){
		esperado = valor_esperado;
	}
}