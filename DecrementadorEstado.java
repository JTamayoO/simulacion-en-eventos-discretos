class DecrementadorEstado implements ModificadorEstado{
	private VariableEstado variable_estado;
	private Double valor_decremento;

	public DecrementadorEstado(VariableEstado variable){
		variable_estado = variable;
		valor_decremento = 1.0;
	}

	public void modificar(){
		variable_estado.setValor(variable_estado.getValor() - valor_decremento);
	}

	public void setVariableEstado(VariableEstado variable){
		variable_estado = variable;
	}

	public void setValorIncremento(Double valor_incrementar){
		valor_decremento = valor_incrementar;
	}
}