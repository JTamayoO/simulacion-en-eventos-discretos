class VariableEstado implements Comparable{
	private Double valor;
	private String label;

	public VariableEstado(){
		valor = 0.0;
		label = "VAR";
	}

	public void setValor(Double nuevo_valor){
		valor = nuevo_valor;
	}	

	public void setLabel(String nuevo_label){
		label = nuevo_label;
	}

	public int compareTo(Object valor_comparacion){
		int resultado = 0;
		Double valor_comparar = (Double)valor_comparacion;
		if(valor < valor_comparar){
			resultado = -1;
		}else{
			if(valor > valor_comparar){
				resultado = 1;
			}
		}
		return resultado;
	}
	
	public Double getValor(){
		return valor;
	}
	
	public String getLabel(){
		return label;
	}
	
	public String toString(){
		return " "+this.label+ " : " +this.valor;
	}

}