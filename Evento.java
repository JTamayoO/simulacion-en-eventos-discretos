import java.util.ArrayList;

class Evento {
	private ArrayList<Programacion> programaciones;
	private ArrayList<VariableEstado> variables_estado;
	private ArrayList<ModificadorEstado> modificador_estado;
	private Iterador<Programacion> siguiente_programacion;
	private String label;
	private Double tiempo;

	public Evento(){
		programaciones = new ArrayList<Programacion>();
		variables_estado = new ArrayList<VariableEstado>();
		siguiente_programacion = new IteradorProgramacion(programaciones);
		modificador_estado = new ArrayList<ModificadorEstado>();
		tiempo = Double.POSITIVE_INFINITY;
	}


	public void procesarEvento(){
		cambiarVariablesEstado();
		programarEventos();
	}

	public void cambiarVariablesEstado(){
		for(ModificadorEstado variable_sistema:modificador_estado){
			variable_sistema.modificar();
		}
	}

	public void programarEventos(){
		Programacion siguiente_evento;
		Double tiempo_actual_simulacion = this.tiempo;
		while(siguiente_programacion.hasNext()){
			siguiente_evento = siguiente_programacion.next();
			siguiente_evento.programar(tiempo_actual_simulacion);
		}
	}

	public String toString(){
		return label + " " + tiempo;
	}
	
	public void setTiempo(Double nuevo_tiempo){
		tiempo = nuevo_tiempo;
	}
	
	public Double getTiempo(){
		return tiempo;
	}
	
	public void setLabel(String nuevo_label){
		label = nuevo_label;
	}

	public void agregarVariableEstado(VariableEstado nueva_variable){
		variables_estado.add(nueva_variable);
	}

	public void agregarModificadorEstado(ModificadorEstado modificador){
		modificador_estado.add(modificador);
	}

	public void agregarProgramacion(Programacion programacion){
		programaciones.add(programacion);
	}

	public ArrayList<VariableEstado> getVariablesEstado(){
		return variables_estado;
	}

	public String getLabel() {
		return label;
	}
}