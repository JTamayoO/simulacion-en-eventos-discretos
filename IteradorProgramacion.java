import java.util.ArrayList;

class IteradorProgramacion implements Iterador{
	private ArrayList<Programacion> programaciones;
	private int posicion;
	public IteradorProgramacion(ArrayList<Programacion> programaciones){
		this.programaciones = programaciones;
		this.posicion = 0;
	}
	public boolean hasNext(){
		boolean continuar = true;
		int longitud = programaciones.size();
		Programacion siguiente_programacion = null;
		Programacion programacion = null;
					
		for(int i = posicion ; i < longitud; i++){
			programacion = programaciones.get(i);
			if(!programacion.cumpleCondiciones()){
			  continuar = false;
			  posicion++;
			}else{
				continuar = true;
				break;
			}
		}
		
		if(posicion == longitud){
			posicion = 0;
			continuar = false;
			
		}
		
		return continuar;
		
	}
	public Programacion next(){
		Programacion siguiente_programacion = programaciones.get(posicion++);	
		return siguiente_programacion;
	}
}
