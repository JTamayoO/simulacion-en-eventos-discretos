import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;


public class Resultados implements Observer{
	private String cls = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";

	private Queue<Double> ingresos;
	private ArrayList<Double> salidas;
        
    private Queue<Double> ingresos_2;
	private ArrayList<Double> salidas_2;
	
	

	private Double wq;
	private Double lq;
	private Double area;
	private Double ultimo_registro;
        
    private Double wq_2;
	private Double lq_2;
	private Double area_2;
	private Double ultimo_registro_2;

	private Double descartadas_cola_1 = 0.0;
	private Double descartadas_cola_2 = 0.0;

	
	
	private Controlador control;
	public Resultados(){
		control = Controlador.getControl();
		ingresos = new LinkedList<Double>();
		salidas = new ArrayList<Double>();
		wq = 0.0;
		lq = 0.0;
		area = 0.0;
		ultimo_registro = 0.0;
                
                ingresos_2 = new LinkedList<Double>();
		salidas_2 = new ArrayList<Double>();
		wq_2 = 0.0;
		lq_2 = 0.0;
		area_2 = 0.0;
		ultimo_registro_2 = 0.0;
	}
	
	public void mostrarResultados(Double lambda, Double mu, Double l2, Double m2, Double probabilidad){
		int num_salidas = salidas.size();
		Double[] sys_in = new Double[num_salidas];
		Double sum = 0.0;
		for(int i = 0; i < num_salidas; i++){
			Double in = ingresos.poll();
			sum += salidas.get(i) - in;
			sys_in[i] = in;
		}
		
		wq = sum / num_salidas;
		lq = area / control.getTiempo_simulacion_max();
		
                
        int num_salidas_2 = salidas_2.size();
		Double[] sys_out = new Double[num_salidas_2];
		Double sum_2 = 0.0;
		for(int i = 0; i < num_salidas_2; i++){
			Double out = salidas_2.get(i);
			sum_2 += out - ingresos_2.poll();
			sys_out[i] = out;
		}
		
		wq_2 = sum_2 / num_salidas_2;
		lq_2 = area_2 / control.getTiempo_simulacion_max();
                
        
        Double W = 0.0;
        for(int j = 0 ; j < num_salidas_2; j++){
        	W += sys_out[j] - sys_in[j];
        }
        W /= num_salidas_2;
		

		Double ro = lambda/mu;
		Double real_lq = (ro*ro)/(1-ro);
		Double real_wq = real_lq/lambda;
                
        Double lambda_2 = lambda / probabilidad;
        Double ro_2 = lambda_2/m2;
		Double real_lq_2 = (ro_2*ro_2)/(1-ro_2);
		Double real_wq_2 = real_lq_2/mu;
                
		
		System.out.println("\n Sim-Wq: " + wq + " Teorico-Wq "+ real_wq );
		System.out.println("\n Sim-Lq: " + lq + " Teorico-Lq "+ real_lq );

		System.out.println("\n Descartadas Cola 1: " + descartadas_cola_1);
                
        System.out.println("\n Sim-Wq-2: " + wq_2 + " Teorico-Wq-2 "+ real_wq_2 );
		System.out.println("\n Sim-Lq-2: " + lq_2 + " Teorico-Lq-2 "+ real_lq_2 );


		System.out.println("\n Descartadas Cola 1: " + descartadas_cola_2);

		System.out.println("\n W: " + W);
      
	}
	
	@Override
	public void update(Observable arg0, Object evento_actual) {
		Evento evento = (Evento)evento_actual;
		Double tiempo_evento = evento.getTiempo();
		
		if(evento.getLabel().equals("A")){
			Double personas_cola_1 = control.getEstado().getVariables_estado().get(1).getValor();
			if(personas_cola_1 >= 10.0){
				descartadas_cola_1 ++;
			}


		}
		
		//if(evento.getLabel().equals("A")){ 
		if(evento.getLabel().equals("Ap")){
			ingresos.add(tiempo_evento);
			area += control.getEstado().getVariables_estado().get(1).getValor() *
					( tiempo_evento - ultimo_registro);
			ultimo_registro = tiempo_evento;
		}
                
                if(evento.getLabel().equals("D")){ 
			ingresos_2.add(tiempo_evento);
			area_2 += control.getEstado().getVariables_estado().get(3).getValor() *
					( tiempo_evento - ultimo_registro_2);
			ultimo_registro_2 = tiempo_evento;
			Double personas_cola_2 = control.getEstado().getVariables_estado().get(3).getValor();
			if(personas_cola_2 >= 10.0){
				descartadas_cola_2 ++;
			}
		}
                
		if(evento.getLabel().equals("E")){ 
			Double tiempo_salida = evento.getTiempo();
			salidas_2.add(tiempo_salida);
			area_2 += control.getEstado().getVariables_estado().get(3).getValor() *
					( tiempo_evento - ultimo_registro_2);
                        ultimo_registro_2 = tiempo_evento;
		}
                
		if(evento.getLabel().equals("B")){ 
			Double tiempo_salida = evento.getTiempo();
			salidas.add(tiempo_salida);
			area += control.getEstado().getVariables_estado().get(1).getValor() *
					( tiempo_evento - ultimo_registro);
			ultimo_registro = tiempo_evento;
		}
                
                
                
		if(evento.getLabel().equals("C")){
			
		}

		ArrayList<VariableEstado> vars = control.getEstado().getVariables_estado();

		Double en_cola = vars.get(1).getValor();
		Double estado = vars.get(2).getValor();
		
		Double cola_2 = vars.get(3).getValor();
		Double estado_2 = vars.get(4).getValor();
		/*
		System.out.print(cls);
		
			StringBuilder str = new StringBuilder();
			str.append("Tiempo: "+ evento.getTiempo());
			str.append("\nCola 1: \n|");
			for(int i = 1; i <= en_cola; i++){
				str.append(i+" ");
			}
			str.append("\r\n");
			if(estado.equals(1.0)){str.append("*");}
			else str.append("|");
			str.append("\n\nCola 2: \n|");
			for(int i = 1; i <= cola_2; i++){
				str.append(i+" ");
			}
			str.append("\r\n");
			if(estado_2.equals(1.0)){str.append("*");}
			else str.append("|");

			
			//for (int i = 0; i < 5; ++i) System.out.println();
		
		System.out.print(str.toString());
		System.out.print(cls);
		/*
		System.out.println("\n"+evento_actual.toString() +" - "+ control.getEventos().toString());
		System.out.println("\n"+control.getEstado().toString());
		*/		

		
		
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
	
	}
}
