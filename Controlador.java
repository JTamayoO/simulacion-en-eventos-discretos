import java.util.ArrayList;
import java.util.Observer;

class Controlador {
	
	private static Controlador CONTROL = new Controlador();
	
	private Estado estado;
	public Estado getEstado() {
		return estado;
	}

	private ArrayList<Evento> eventos;
	private Iterador<Evento> proximo_evento;
	private Double tiempo_simulacion_max;
	public Double getTiempo_simulacion_max() {
		return tiempo_simulacion_max;
	}

	private ArrayList<Observer> observadores;

	public Controlador(){
		estado = new Estado();
		eventos = new ArrayList<Evento>();
		proximo_evento = new IteradorEventos(eventos);
		tiempo_simulacion_max = 1.0;
		observadores = new ArrayList<Observer>();
	}
	
	public static Controlador getControl(){
		return CONTROL;
	}

		
	public void simular(){
		Evento evento_actual = proximo_evento.next();
		while(evento_actual.getTiempo() < tiempo_simulacion_max){
			
			//System.out.println(" \n "+this.toString());
			//System.out.println(evento_actual.toString() +" - "+ eventos.toString());
			
			notificarObservadores(evento_actual);
			
			evento_actual.procesarEvento();
			evento_actual = proximo_evento.next();
		}
		
	}
	
	public void agregarEvento(Evento evento){
		eventos.add(evento);
	}
	
	
	public ArrayList<Evento> getEventos(){
		return eventos;
	}
	
	public String toString(){
		return estado.toString();
	}
	
	public void setEventos(ArrayList<Evento> eventos){
		this.eventos = eventos;
	}
	
	public void setTiempoMax(Double tiempo_max){
		tiempo_simulacion_max = tiempo_max;
	}

	public void setEstado(Estado estado_sistema) {
		estado = estado_sistema;
	}
	
	public void agregarObservadores(Observer observador){
		observadores.add(observador);
	}
	
	public void eliminarObservador(Observer observador){
		//Not implemented yet
	}
	
	public void notificarObservadores(Object evento){
		for(Observer observador : observadores){
			observador.update(null,evento);
		}
	}

}