import java.util.Observer;


class DiscreteEventSimulator {
	public static void main(String[] args) {
	   // colaSimple();
       // colaImplementada();
        colaDobleFinita();
           
	}
        public static void colaDobleFinita(){
                
                Double max_usuarios_cola_1 = 500000.0;
                Double max_usuarios_cola_2 = 500000.0;
               	Double lambda = 50.0;
                Double mu = 70.0;
                Double tiempo_max = 5000.0;
                Double probabilidad = 1.0;
                Double mu_2 = 120.0;


		
		Observer resultados = new Resultados(); 
		Controlador controlador = Controlador.getControl();
                controlador.agregarObservadores(resultados);

                Evento evento_A = new Evento();
                    evento_A.setLabel("A");
                Evento evento_Ap = new Evento();
                	evento_Ap.setLabel("Ap");
                Evento evento_B = new Evento();
                    evento_B.setLabel("B");
                Evento evento_C = new Evento();
                    evento_C.setLabel("C");
                Evento evento_D = new Evento();
                    evento_D.setLabel("D");
                Evento evento_E = new Evento();
                    evento_E.setLabel("E");
                Evento evento_F = new Evento();
                    evento_F.setLabel("F");



            evento_A.setTiempo(0.0);
            // Crear y Agregar Variables Estado
            VariableEstado total_personas = new VariableEstado();
                total_personas.setLabel("N");
            VariableEstado personas_cola = new VariableEstado();
                personas_cola.setLabel("nq");
            VariableEstado estado_servidor = new VariableEstado();
                estado_servidor.setLabel("st");
            VariableEstado papeles = new VariableEstado();
                papeles.setLabel("p");
           
                        
            VariableEstado personas_cola_2 = new VariableEstado();
                personas_cola_2.setLabel("nq_2");
            VariableEstado estado_servidor_2 = new VariableEstado();
                estado_servidor_2.setLabel("st_2");
                        
			
            Estado estado_sistema = new Estado();

            estado_sistema.agregarVariableEstado(total_personas);
            estado_sistema.agregarVariableEstado(personas_cola);
            estado_sistema.agregarVariableEstado(estado_servidor);
            estado_sistema.agregarVariableEstado(personas_cola_2);
            estado_sistema.agregarVariableEstado(estado_servidor_2);
            estado_sistema.agregarVariableEstado(papeles);

            evento_A.agregarVariableEstado(total_personas);

            evento_Ap.agregarVariableEstado(personas_cola);         

            evento_B.agregarVariableEstado(estado_servidor);
            evento_B.agregarVariableEstado(personas_cola);

            evento_C.agregarVariableEstado(estado_servidor);
                        
            evento_D.agregarVariableEstado(personas_cola_2);
            
            evento_E.agregarVariableEstado(personas_cola_2);
            evento_E.agregarVariableEstado(estado_servidor_2);
            
            evento_F.agregarVariableEstado(estado_servidor_2);
                        

			// Agregar Modificadores Estado

                        
			evento_A.agregarModificadorEstado(new IncrementadorEstado(total_personas));
			evento_Ap.agregarModificadorEstado(new IncrementadorEstado(personas_cola));

			ModificadorEstado switch_servidor = new SwitchEstado(estado_servidor);	
			
                        evento_B.agregarModificadorEstado(switch_servidor);
			evento_B.agregarModificadorEstado(new DecrementadorEstado(personas_cola));

			evento_C.agregarModificadorEstado(switch_servidor);
                        
                        evento_D.agregarModificadorEstado(new IncrementadorEstado(personas_cola_2));
                        
                        ModificadorEstado switch_servidor_2= new SwitchEstado(estado_servidor_2);
                        evento_E.agregarModificadorEstado(switch_servidor_2);
                        evento_E.agregarModificadorEstado(new DecrementadorEstado(personas_cola_2));
                        
                        evento_F.agregarModificadorEstado(switch_servidor_2);
                        evento_F.agregarModificadorEstado(new RandomEstado(papeles));


			/// Crear y agregar Programaciones

			//A --> Ap
			Programacion programacion_A_Ap = new Programacion();
				programacion_A_Ap.setEvento(evento_Ap);
				//nill dist

				Condicion limite_usuarios_cola_1 = new Condicion();
                limite_usuarios_cola_1.setVariableEstado(personas_cola);
                limite_usuarios_cola_1.setResultado(max_usuarios_cola_1);
                limite_usuarios_cola_1.setEsperado(Condicion.MENOR);

                programacion_A_Ap.agregarCondicion(limite_usuarios_cola_1);

             evento_A.agregarProgramacion(programacion_A_Ap);



			// A --> A
			Programacion programacion_B = new Programacion();
				programacion_B.setEvento(evento_A);
				programacion_B.setDistribucion(new DistribucionExponencial(lambda));
					
			evento_A.agregarProgramacion(programacion_B);


			// Ap --> B
			Programacion programacion_Ap_B = new Programacion();
				programacion_Ap_B.setEvento(evento_B);
				// Default : Nill distribution.
				
				Condicion servidor_libre = new Condicion();
					servidor_libre.setVariableEstado(estado_servidor);
					servidor_libre.setResultado(0.0);
					servidor_libre.setEsperado(Condicion.IGUAL);
      
                                   
				programacion_Ap_B.agregarCondicion(servidor_libre);
                                

			evento_Ap.agregarProgramacion(programacion_Ap_B);

			//Ap --> Ap
			Programacion desprogramar_Ap = new Programacion();
				desprogramar_Ap.setEvento(evento_Ap);
				desprogramar_Ap.setDistribucion(new DistribucionInfinity());

			evento_Ap.agregarProgramacion(desprogramar_Ap);


			// B --> B
			Programacion desprogramar_b = new Programacion();
				desprogramar_b.setEvento(evento_B);
				desprogramar_b.setDistribucion(new DistribucionInfinity());
			evento_B.agregarProgramacion(desprogramar_b);
			
			// B --> C
			Programacion programacion_C = new Programacion();
				programacion_C.setEvento(evento_C);
				programacion_C.setDistribucion(new DistribucionExponencial(mu));
				//Sin condicion
			
			evento_B.agregarProgramacion(programacion_C);
			
			// C --> C
			Programacion desprogramar_c = new Programacion();
				desprogramar_c.setEvento(evento_C);
				desprogramar_c.setDistribucion(new DistribucionInfinity());
			evento_C.agregarProgramacion(desprogramar_c);

			// C --> B
			Programacion programacion_D = new Programacion();
				programacion_D.setEvento(evento_B);
				// Default : Nill distribution.

				Condicion cola_no_vacia = new Condicion();
					cola_no_vacia.setVariableEstado(personas_cola);
					cola_no_vacia.setResultado(0.0);
					cola_no_vacia.setEsperado(Condicion.MAYOR);

				programacion_D.agregarCondicion(cola_no_vacia);
			
			evento_C.agregarProgramacion(programacion_D);
                        
                        // C --> D
                        
                        Programacion programacion_E = new Programacion();
				programacion_E.setEvento(evento_D);
				//programacion_E.setDistribucion(new DistribucionExponencial(lambda));
				// Condicion limite usuarios
                                Condicion limite_usuarios_cola_2 = new Condicion();
                                        limite_usuarios_cola_2.setVariableEstado(personas_cola_2);
                                        limite_usuarios_cola_2.setResultado(max_usuarios_cola_2);
                                        limite_usuarios_cola_2.setEsperado(Condicion.MENOR);
                                        
                                programacion_E.agregarCondicion(limite_usuarios_cola_2);
			
			evento_C.agregarProgramacion(programacion_E);
                        
                        // D --> D
                        Programacion desprogramar_D = new Programacion();
				desprogramar_D.setEvento(evento_D);
				desprogramar_D.setDistribucion(new DistribucionInfinity());
			evento_D.agregarProgramacion(desprogramar_D);
                        
                        // D --> E
                        Programacion programacion_F = new Programacion();
				programacion_F.setEvento(evento_E);
				// Default : Nill distribution.
                        Condicion servidor_libre_2 = new Condicion();
					servidor_libre_2.setVariableEstado(estado_servidor_2);
					servidor_libre_2.setResultado(0.0);
					servidor_libre_2.setEsperado(Condicion.IGUAL);

				programacion_F.agregarCondicion(servidor_libre_2);

			evento_D.agregarProgramacion(programacion_F);
                        
                        // E --> E
                        Programacion desprogramar_E = new Programacion();
				desprogramar_E.setEvento(evento_E);
				desprogramar_E.setDistribucion(new DistribucionInfinity());
			evento_E.agregarProgramacion(desprogramar_E);
                        
                        // E --> F
                        Programacion programacion_G = new Programacion();
				programacion_G.setEvento(evento_F);
				programacion_G.setDistribucion(new DistribucionExponencial(mu_2));
				//Sin condicion
			
			evento_E.agregarProgramacion(programacion_G);
                        
                        // F --> F
                        
                         Programacion desprogramar_F = new Programacion();
				desprogramar_F.setEvento(evento_F);
				desprogramar_F.setDistribucion(new DistribucionInfinity());
			evento_F.agregarProgramacion(desprogramar_F);
                        
                        // F --> E
                        Programacion programacion_H = new Programacion();
				programacion_H.setEvento(evento_E);
				// Default : Nill distribution.

				Condicion cola_no_vacia_2 = new Condicion();
					cola_no_vacia_2.setVariableEstado(personas_cola_2);
					cola_no_vacia_2.setResultado(0.0);
					cola_no_vacia_2.setEsperado(Condicion.MAYOR);

				programacion_H.agregarCondicion(cola_no_vacia_2);
			
			evento_F.agregarProgramacion(programacion_H);
                        
                        // F --> D
                        Programacion programacion_I = new Programacion();
				programacion_I.setEvento(evento_D);
				//programacion_I.setDistribucion(new DistribucionUniforme()); // Revisar Probabilidad
				//Sin condicion
                                                              
                                Condicion no_tiene_papeles = new Condicion();
                                    no_tiene_papeles.setVariableEstado(papeles);
                                    no_tiene_papeles.setResultado(probabilidad);
                                    no_tiene_papeles.setEsperado(Condicion.MAYOR);
                                   
                                programacion_I.agregarCondicion(no_tiene_papeles);
			
			evento_F.agregarProgramacion(programacion_I);

			
		controlador.agregarEvento(evento_A);
		controlador.agregarEvento(evento_Ap);
		controlador.agregarEvento(evento_B);
		controlador.agregarEvento(evento_C);
                controlador.agregarEvento(evento_D);
		controlador.agregarEvento(evento_E);
		controlador.agregarEvento(evento_F);
		
		controlador.setEstado(estado_sistema);
		
		controlador.setTiempoMax(tiempo_max);
		
		controlador.simular();
		
		((Resultados)resultados).mostrarResultados(lambda, mu, lambda, mu_2, probabilidad);
        }
        public static void colaImplementada()
        {
            
                Double lambda = 50.0;
                Double mu = 70.0;
                Double tiempo_max = 500.0;
                Double probabilidad = 1.0;
                Double mu_2 = 120.0;
		
		Observer resultados = new Resultados(); 
		Controlador controlador = Controlador.getControl();
                controlador.agregarObservadores(resultados);

                Evento evento_A = new Evento();
                    evento_A.setLabel("A");
                Evento evento_B = new Evento();
                    evento_B.setLabel("B");
                Evento evento_C = new Evento();
                    evento_C.setLabel("C");
                Evento evento_D = new Evento();
                    evento_D.setLabel("D");
                Evento evento_E = new Evento();
                    evento_E.setLabel("E");
                Evento evento_F = new Evento();
                    evento_F.setLabel("F");



            evento_A.setTiempo(0.0);
            // Crear y Agregar Variables Estado
            VariableEstado total_personas = new VariableEstado();
                total_personas.setLabel("N");
            VariableEstado personas_cola = new VariableEstado();
                personas_cola.setLabel("nq");
            VariableEstado estado_servidor = new VariableEstado();
                estado_servidor.setLabel("st");
            VariableEstado papeles = new VariableEstado();
                papeles.setLabel("p");
                        
            VariableEstado personas_cola_2 = new VariableEstado();
                personas_cola_2.setLabel("nq_2");
            VariableEstado estado_servidor_2 = new VariableEstado();
                estado_servidor_2.setLabel("st_2");
                        
			
            Estado estado_sistema = new Estado();

            estado_sistema.agregarVariableEstado(total_personas);
            estado_sistema.agregarVariableEstado(personas_cola);
            estado_sistema.agregarVariableEstado(estado_servidor);
            estado_sistema.agregarVariableEstado(personas_cola_2);
            estado_sistema.agregarVariableEstado(estado_servidor_2);
            estado_sistema.agregarVariableEstado(papeles);

            evento_A.agregarVariableEstado(total_personas);
            evento_A.agregarVariableEstado(personas_cola);

            evento_B.agregarVariableEstado(estado_servidor);
            evento_B.agregarVariableEstado(personas_cola);

            evento_C.agregarVariableEstado(estado_servidor);
                        
            evento_D.agregarVariableEstado(personas_cola_2);
            
            evento_E.agregarVariableEstado(personas_cola_2);
            evento_E.agregarVariableEstado(estado_servidor_2);
            
            evento_F.agregarVariableEstado(estado_servidor_2);
                        

			// Agregar Modificadores Estado

                        
			evento_A.agregarModificadorEstado(new IncrementadorEstado(total_personas));
			evento_A.agregarModificadorEstado(new IncrementadorEstado(personas_cola));

			ModificadorEstado switch_servidor = new SwitchEstado(estado_servidor);	
			
                        evento_B.agregarModificadorEstado(switch_servidor);
			evento_B.agregarModificadorEstado(new DecrementadorEstado(personas_cola));

			evento_C.agregarModificadorEstado(switch_servidor);
                        
                        evento_D.agregarModificadorEstado(new IncrementadorEstado(personas_cola_2));
                        
                        ModificadorEstado switch_servidor_2= new SwitchEstado(estado_servidor_2);
                        evento_E.agregarModificadorEstado(switch_servidor_2);
                        evento_E.agregarModificadorEstado(new DecrementadorEstado(personas_cola_2));
                        
                        evento_F.agregarModificadorEstado(switch_servidor_2);
                        evento_F.agregarModificadorEstado(new RandomEstado(papeles));


			/// Crear y agregar Programaciones

			// A --> B
			Programacion programacion_A = new Programacion();
				programacion_A.setEvento(evento_B);
				// Default : Nill distribution.
				
				Condicion servidor_libre = new Condicion();
					servidor_libre.setVariableEstado(estado_servidor);
					servidor_libre.setResultado(0.0);
					servidor_libre.setEsperado(Condicion.IGUAL);

				programacion_A.agregarCondicion(servidor_libre);

			evento_A.agregarProgramacion(programacion_A);
			// A --> A
			Programacion programacion_B = new Programacion();
				programacion_B.setEvento(evento_A);
				programacion_B.setDistribucion(new DistribucionExponencial(lambda));
				// Sin condicion
			
			evento_A.agregarProgramacion(programacion_B);
			
			// B --> B
			Programacion desprogramar_b = new Programacion();
				desprogramar_b.setEvento(evento_B);
				desprogramar_b.setDistribucion(new DistribucionInfinity());
			evento_B.agregarProgramacion(desprogramar_b);
			
			// B --> C
			Programacion programacion_C = new Programacion();
				programacion_C.setEvento(evento_C);
				programacion_C.setDistribucion(new DistribucionExponencial(mu));
				//Sin condicion
			
			evento_B.agregarProgramacion(programacion_C);
			
			// C --> C
			Programacion desprogramar_c = new Programacion();
				desprogramar_c.setEvento(evento_C);
				desprogramar_c.setDistribucion(new DistribucionInfinity());
			evento_C.agregarProgramacion(desprogramar_c);

			// C --> B
			Programacion programacion_D = new Programacion();
				programacion_D.setEvento(evento_B);
				// Default : Nill distribution.

				Condicion cola_no_vacia = new Condicion();
					cola_no_vacia.setVariableEstado(personas_cola);
					cola_no_vacia.setResultado(0.0);
					cola_no_vacia.setEsperado(Condicion.MAYOR);

				programacion_D.agregarCondicion(cola_no_vacia);
			
			evento_C.agregarProgramacion(programacion_D);
                        
                        // C --> D
                        
                        Programacion programacion_E = new Programacion();
				programacion_E.setEvento(evento_D);
				//programacion_E.setDistribucion(new DistribucionExponencial(lambda));
				// Sin condicion
			
			evento_C.agregarProgramacion(programacion_E);
                        
                        // D --> D
                        Programacion desprogramar_D = new Programacion();
				desprogramar_D.setEvento(evento_D);
				desprogramar_D.setDistribucion(new DistribucionInfinity());
			evento_D.agregarProgramacion(desprogramar_D);
                        
                        // D --> E
                        Programacion programacion_F = new Programacion();
				programacion_F.setEvento(evento_E);
				// Default : Nill distribution.
                        Condicion servidor_libre_2 = new Condicion();
					servidor_libre_2.setVariableEstado(estado_servidor_2);
					servidor_libre_2.setResultado(0.0);
					servidor_libre_2.setEsperado(Condicion.IGUAL);

				programacion_F.agregarCondicion(servidor_libre_2);

			evento_D.agregarProgramacion(programacion_F);
                        
                        // E --> E
                        Programacion desprogramar_E = new Programacion();
				desprogramar_E.setEvento(evento_E);
				desprogramar_E.setDistribucion(new DistribucionInfinity());
			evento_E.agregarProgramacion(desprogramar_E);
                        
                        // E --> F
                        Programacion programacion_G = new Programacion();
				programacion_G.setEvento(evento_F);
				programacion_G.setDistribucion(new DistribucionExponencial(mu_2));
				//Sin condicion
			
			evento_E.agregarProgramacion(programacion_G);
                        
                        // F --> F
                        
                         Programacion desprogramar_F = new Programacion();
				desprogramar_F.setEvento(evento_F);
				desprogramar_F.setDistribucion(new DistribucionInfinity());
			evento_F.agregarProgramacion(desprogramar_F);
                        
                        // F --> E
                        Programacion programacion_H = new Programacion();
				programacion_H.setEvento(evento_E);
				// Default : Nill distribution.

				Condicion cola_no_vacia_2 = new Condicion();
					cola_no_vacia_2.setVariableEstado(personas_cola_2);
					cola_no_vacia_2.setResultado(0.0);
					cola_no_vacia_2.setEsperado(Condicion.MAYOR);

				programacion_H.agregarCondicion(cola_no_vacia_2);
			
			evento_F.agregarProgramacion(programacion_H);
                        
                        // F --> D
                        Programacion programacion_I = new Programacion();
				programacion_I.setEvento(evento_D);
				//programacion_I.setDistribucion(new DistribucionUniforme()); // Revisar Probabilidad
				//Sin condicion
                                                              
                                Condicion no_tiene_papeles = new Condicion();
                                    no_tiene_papeles.setVariableEstado(papeles);
                                    no_tiene_papeles.setResultado(probabilidad);
                                    no_tiene_papeles.setEsperado(Condicion.MAYOR);
                                   
                                programacion_I.agregarCondicion(no_tiene_papeles);
			
			evento_F.agregarProgramacion(programacion_I);

			
		controlador.agregarEvento(evento_A);
		controlador.agregarEvento(evento_B);
		controlador.agregarEvento(evento_C);
                controlador.agregarEvento(evento_D);
		controlador.agregarEvento(evento_E);
		controlador.agregarEvento(evento_F);
		
		controlador.setEstado(estado_sistema);
		
		controlador.setTiempoMax(tiempo_max);
		
		controlador.simular();
		
		((Resultados)resultados).mostrarResultados(lambda, mu, lambda, mu_2, probabilidad);
        }

	public static void colaSimple(){

		Double lambda = 50.0;
		Double mu = 100.0;
		Double tiempo_max = 10.0;
		
		Observer resultados = new Resultados(); 
		Controlador controlador = Controlador.getControl();
			
			controlador.agregarObservadores(resultados);
			
			Evento ingreso_sistema = new Evento();
				ingreso_sistema.setLabel("A");
			Evento inicio_atencion = new Evento();
				inicio_atencion.setLabel("B");
			Evento fin_atencion = new Evento();
				fin_atencion.setLabel("C");
				
			ingreso_sistema.setTiempo(0.0);
			
			// Crear y Agregar Variables Estado
			VariableEstado total_personas = new VariableEstado();
			total_personas.setLabel("N");
			VariableEstado personas_cola = new VariableEstado();
			personas_cola.setLabel("nq");
			VariableEstado estado_servidor = new VariableEstado();
			estado_servidor.setLabel("st");
			
			Estado estado_sistema = new Estado();
			estado_sistema.agregarVariableEstado(total_personas);
			estado_sistema.agregarVariableEstado(personas_cola);
			estado_sistema.agregarVariableEstado(estado_servidor);
			
			ingreso_sistema.agregarVariableEstado(total_personas);
			ingreso_sistema.agregarVariableEstado(personas_cola);

			inicio_atencion.agregarVariableEstado(estado_servidor);
			inicio_atencion.agregarVariableEstado(personas_cola);

			fin_atencion.agregarVariableEstado(estado_servidor);

			// Agregar Modificadores Estado

			ingreso_sistema.agregarModificadorEstado(new IncrementadorEstado(total_personas));
			ingreso_sistema.agregarModificadorEstado(new IncrementadorEstado(personas_cola));

			ModificadorEstado switch_servidor = new SwitchEstado(estado_servidor);	
			inicio_atencion.agregarModificadorEstado(switch_servidor);
			inicio_atencion.agregarModificadorEstado( new DecrementadorEstado(personas_cola));

			fin_atencion.agregarModificadorEstado(switch_servidor);


			/// Crear y agregar Programaciones

			// A --> B
			Programacion ingreso_inicio_atencion = new Programacion();
				ingreso_inicio_atencion.setEvento(inicio_atencion);
				// Default : Nill distribution.
				
				Condicion servidor_libre = new Condicion();
					servidor_libre.setVariableEstado(estado_servidor);
					servidor_libre.setResultado(0.0);
					servidor_libre.setEsperado(Condicion.IGUAL);

				ingreso_inicio_atencion.agregarCondicion(servidor_libre);

			ingreso_sistema.agregarProgramacion(ingreso_inicio_atencion);
			// A --> A
			Programacion ingreso_ingreso = new Programacion();
				ingreso_ingreso.setEvento(ingreso_sistema);
				ingreso_ingreso.setDistribucion(new DistribucionExponencial(lambda));
				// Sin condicion
			
			ingreso_sistema.agregarProgramacion(ingreso_ingreso);
			
			// B --> B
			Programacion desprogramar_b = new Programacion();
				desprogramar_b.setEvento(inicio_atencion);
				desprogramar_b.setDistribucion(new DistribucionInfinity());
			inicio_atencion.agregarProgramacion(desprogramar_b);
			
			// B --> C
			Programacion inicio_atencion_fin = new Programacion();
				inicio_atencion_fin.setEvento(fin_atencion);
				inicio_atencion_fin.setDistribucion(new DistribucionExponencial(mu));
				//Sin condicion
			
			inicio_atencion.agregarProgramacion(inicio_atencion_fin);
			
			// C --> C
			Programacion desprogramar_c = new Programacion();
				desprogramar_c.setEvento(fin_atencion);
				desprogramar_c.setDistribucion(new DistribucionInfinity());
			fin_atencion.agregarProgramacion(desprogramar_c);

			// C --> B
                        
			Programacion fin_atencion_inicio = new Programacion();
				fin_atencion_inicio.setEvento(inicio_atencion);
				// Default : Nill distribution.

				Condicion cola_no_vacia = new Condicion();
					cola_no_vacia.setVariableEstado(personas_cola);
					cola_no_vacia.setResultado(0.0);
					cola_no_vacia.setEsperado(Condicion.MAYOR);

				fin_atencion_inicio.agregarCondicion(cola_no_vacia);
			
			fin_atencion.agregarProgramacion(fin_atencion_inicio);
				

			
		controlador.agregarEvento(ingreso_sistema);
		controlador.agregarEvento(inicio_atencion);
		controlador.agregarEvento(fin_atencion);
		
		controlador.setEstado(estado_sistema);
		
		controlador.setTiempoMax(tiempo_max);
		
		controlador.simular();
		
		//((Resultados)resultados).mostrarResultados(lambda, mu);
	}
}