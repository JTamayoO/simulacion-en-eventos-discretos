/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jonathan tamayo
 */
public class RandomEstado implements ModificadorEstado{
    private VariableEstado variable_estado;
    private Double probabilidad;
    
    public RandomEstado(VariableEstado var_estado){
        variable_estado = var_estado;
        probabilidad = Math.random();
    }
    
    @Override
    public void modificar() {
        probabilidad = Math.random();
        variable_estado.setValor(probabilidad);
    }
    
    public Double getProbabilidad(){
        return probabilidad;
    }
    
}
